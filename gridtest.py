import torch
import torchvision
import matplotlib.pyplot as plt
from matplotlib import colors
import numpy as np
from utils import open_image, transform_to_tensor

def plot_index(index):
    binary = format(index, '025b')
    # map to interger array
    vector = list(map(int,binary))
    data = np.array(vector)
    data = np.reshape(data, (5,5)) # we now have a matrix

    #print(data)

    # create color map
    cmap = colors.ListedColormap(['blue','red'])
    bounds = [0,1,2]
    norm = colors.BoundaryNorm(bounds, cmap.N)

    fig, ax = plt.subplots()
    ax.imshow(data, cmap=cmap, norm=norm)
    plt.show()


def plot_index_grids(samples, predicted=0):
    n = len(samples)
    f, axarr = plt.subplots(1,n)
    
    for i in range(n):
        binary = format(samples[i].item(), '025b')
        vector = list(map(int,binary))
        data = np.array(vector)
        data = np.reshape(data, (5,5)) # we now have a matrix
        # create color map
        cmap = colors.ListedColormap(['blue','red'])
        bounds = [0,1,2]
        norm = colors.BoundaryNorm(bounds, cmap.N)
        if predicted == i:
            axarr[i].title.set_text('predicted')
        axarr[i].imshow(data, cmap=cmap, norm=norm)

    plt.show()




if __name__ == '__main__':

    # pil = open_image('data/covid/test/COVID-19/COVID-19(2).png')

    # format(value, '025b') will ouput a binary string for 'value' in 25 chars
    index = 865470
    binary = format(index, '025b')
    # map to interger array
    vector = list(map(int,binary))
    data = np.array(vector)
    data = np.reshape(data, (5,5)) # we now have a matrix

    print(data)

    # create color map
    cmap = colors.ListedColormap(['blue','red'])
    bounds = [0,1,2]
    norm = colors.BoundaryNorm(bounds, cmap.N)

    fig, ax = plt.subplots()
    ax.imshow(data, cmap=cmap, norm=norm)
    plt.show()

import torch
import torchvision
from torch import nn


class ResNetGCML(nn.Module):
    def __init__(self, fc_classes):
        super(ResNetGCML, self).__init__()
        # we use resnet50 as the original feature extractor
        self.classes = fc_classes
        self.base = torchvision.models.resnet50(pretrained=True)
        # now we slice the network and add additional layers (extra convolution and fc)
        self.conv1 = self.base.conv1
        self.bn1 = self.base.bn1
        self.relu = self.base.relu
        self.maxpool = self.base.maxpool
        # now the 4 layers of residual blocks
        self.layer1 = self.base.layer1
        self.layer2 = self.base.layer2
        self.layer3 = self.base.layer3
        self.layer4 = self.base.layer4
        # last convolutional layer will downsample our activations to 5x5 from 7x7
        self.conv2 = nn.Conv2d(2048, 2048, kernel_size=3, stride=1, padding=0)
        self.relu2 = nn.ReLU(inplace=True)
        # GAP layer
        self.avgpool = nn.AdaptiveAvgPool2d((1,1))
        self.fc = nn.Linear(2048, self.classes)

    # define our forward method
    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)
        x = self.conv2(x)
        x = self.relu2(x)
        x = self.avgpool(x)
        x = torch.flatten(x,1)
        x = self.fc(x)

        return x

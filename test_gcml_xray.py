import torch
from torchvision import datasets, transforms
import torch.nn.functional as F
import os
import metrx as mx
import utils as u


# Device configuration
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# Hyper-parameters
num_epochs = 1
learning_rate = 0.001
cam_activation_point = 0.05
# this depends on the final resolution map size of the CNN, and is just h*w of it.
gcl_length = 25


# DATA_DIR = 'data/expcovid'
DATA_DIR = 'data/covid'
LOG_DIR = 'logs'
# progress file name will be written to the logs dir
BATCH_SIZE = 12
WORKERS = 12
IMAGE_NET_MEAN = [0.485, 0.456, 0.406]
IMAGE_NET_STD = [0.229, 0.224, 0.225]

# data setup
# data processing, adding additional sets and transforms, i.e. test corresponds to a directory
# and will create appropriate datasets
data_transforms = {
    # 'train': transforms.Compose([
    #     transforms.RandomResizedCrop(224),
    #     #transforms.RandomHorizontalFlip(),
    #     transforms.ToTensor(),
    #     transforms.Normalize(IMAGE_NET_MEAN, IMAGE_NET_STD)

    # ]),
    # 'val': transforms.Compose([
    #     transforms.Resize(224),
    #     transforms.CenterCrop(224),
    #     transforms.ToTensor(),
    #     transforms.Normalize(IMAGE_NET_MEAN, IMAGE_NET_STD)
    # ]),
    'test': transforms.Compose([
        transforms.Resize(224),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(IMAGE_NET_MEAN, IMAGE_NET_STD)
    ])
}

# use the ImageFolder class from torchvision that accepts datasets structured by datax.py
# create datasets with appropriate transformation
image_datasets = {x: datasets.ImageFolder(os.path.join(DATA_DIR, x), data_transforms[x]) for x in data_transforms}
# create dataloaders
dataloaders = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size=BATCH_SIZE, shuffle=True, num_workers=WORKERS) for x in data_transforms}

dataset_sizes = {x: len(image_datasets[x]) for x in data_transforms}
class_names = image_datasets['test'].classes
number_classes = len(class_names)



class Hook():
    def __init__(self, module, backward=False):
        if backward==False:
            self.hook = module.register_forward_hook(self.hook_fn)
        else:
            self.hook = module.register_backward_hook(self.hook_fn)
    def hook_fn(self, module, input, output):
        self.input = input
        self.output = output
    def close(self):
        self.hook.remove()

def compute_cams(activations, labels, weights):
    # get number of batches
    cams = []
    gcl_index = []
    batches,_,_,_ = activations.size()
    for b in range(batches):
        cw = weights[labels[b],:] # get class weights for label[b]
        a = activations[b,:]
        # dot product between class weights vector and filter activations
        cam = (cw.view(-1,1,1) * a).sum(0)
        cam = F.relu(cam)
        # normalize
        cam_min, cam_max = cam.min(), cam.max()
        cam = (cam - cam_min).div(cam_max - cam_min)
        cams.append(cam)
        # print('CAM:\n',cam)
        # n,m = cam.size()
        index = cam_to_index(cam.view(-1), gcl_length)
        gcl_index.append(index.item())
        #print("Index: ", index.item())

    return cams, gcl_index


def compute_cam_classes(activations, weights, gcltable):
    # get number of batches
    gcl_index = []
    batches,_,_,_ = activations.size()
    # number of classes in this CNN
    classes, _ = weights.size()
    index_matrix = torch.zeros([batches, classes], dtype=torch.int32)
    probs_matrix = torch.zeros(index_matrix.size(), dtype=torch.float32)
    
    for b in range(batches):
        
        a = activations[b,:]
        # compute cams for each class
        
        for c in range(classes):
            cw = weights[c,:] # get class weights for class c
            # dot product between class weights vector and filter activations
            cam = (cw.view(-1,1,1) * a).sum(0)
            cam = F.relu(cam)
            # normalize
            cam_min, cam_max = cam.min(), cam.max()
            cam = (cam - cam_min).div(cam_max - cam_min)
            index = cam_to_index(cam.view(-1), gcl_length)
            #print("Index: ", index)
            index_matrix[b][c] = index
            # get the likelihood probability
            probs_matrix[b][c] = gcltable[c][index]
    return index_matrix, probs_matrix

# takes a 2D tensor and computes an index for global correlation vector,
# which is a powerset of all positive CAM activations
def cam_to_index(cam, bits):
    # cam_activation_point is the threshhold on cam activations
    a = torch.where(cam > cam_activation_point, 1, 0)
    b = a.view(-1)
    mask = 2 ** torch.arange(bits - 1, -1, -1).to(b.device, b.dtype)
    return torch.sum(mask * b, -1)



if __name__ == '__main__':

    # Load a given GCML structure, make sure to keep track of tau (threshold)
    # gcl = torch.load('gcml/gclxray05.pt') # 93.29
    # gcl = torch.load('gcml/gclxray03_val.pt') # accuracy 93.976
    gcl = torch.load('gcml/gclxray05_val15.pt') # 94.148

    # normalize to probs, since we hold counts to enable continued training
    x,_ = gcl.size()
    for y in range(x):
        gcl[y] = gcl[y] / gcl[y].sum(0)    

    gcl = gcl.to(device)
    # Load the feature extractor that we trained
    model = torch.load('models/resGCML.pt')
    model.eval()
    model = model.to(device)
    # PUT THIS BACK IN WHEN RUNNING MODELS THAT MATCH CLASS COUNTS
    number_classes = model.fc.weight.size()[0]
    hook = Hook(model.relu2)
    z = 0

    total = 0
    correct = 0
    correct_fl = 0
    correct_hybrid = 0
    confusion_matrix = torch.zeros(number_classes, number_classes)
    confusion_matrix_fc = torch.zeros(number_classes, number_classes)
    with torch.no_grad(): 
        for i, (images, labels) in enumerate(dataloaders['test']):
            images = images.to(device)
            labels = labels.to(device)
            outputs = model(images)

            # correct on cnn preds
            _, pred = torch.max(outputs.data, 1)
            correct_fl += (pred == labels).sum().item()
            # print("predic: ", pred)
            # print("Labels: ", labels)
            for t, p in zip(pred.view(-1), labels.view(-1)):
                confusion_matrix_fc[t.long(), p.long()] += 1

            #matrix = compute_cams(hook.output, labels, model.fc.weight)
            matrix, probs = compute_cam_classes(hook.output, model.fc.weight, gcl)
            #print("Likelihoods:\n", probs)
            values, predicted = probs.max(1)
            predicted = predicted.to(device)
            # compute accuracy
            total += labels.size(0)
            correct += (predicted == labels).sum().item()

            # confusion matrix
            # confusion matrix calculation
            for t, p in zip(predicted.view(-1), labels.view(-1)):
                confusion_matrix[t.long(), p.long()] += 1
            #break
         # REPORT STATISTICS
        # print("Confusion Matrix\n", confusion_matrix)
        print('GCML Accuracy: {} %'.format(100 * correct / total))
        print("GCML Class Acc: ", mx.per_class_accuracy(confusion_matrix))
        print('CNN Accuracy: {} %'.format(100 * correct_fl / total))
        print('CNN Class Acc: ', mx.per_class_accuracy(confusion_matrix_fc))
        # print('Conf Matrix FC\n', confusion_matrix_fc)
        print('GCML F1: ', mx.combinedF1(confusion_matrix))
        print('CNN F1: ', mx.combinedF1(confusion_matrix_fc))
        print('GCML SEN: ', mx.combinedSensitivity(confusion_matrix))
        print('CNN SEN: ', mx.combinedSensitivity(confusion_matrix_fc))

        # to compute CI for given samples use metrx.get_ci() method (see metrx.py)

         # display the conf matrix
        class_names = ['COVID-19', 'No Finding', 'Viral Pneumonia']
        cm = confusion_matrix.int()
        cmf = confusion_matrix_fc.int()
        u.plot_confusion_matrix(cm, class_names, title='GCML Confusion Matrix')
        u.plot_confusion_matrix(cmf, class_names, title='CNN Confusion Matrix')
import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn.functional as F
import metrx as mx
import utils as u


# Device configuration
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
# TAU value
cam_activation_point = 0.001
# this depends on the final resolution map size of the CNN, and is just h*w of it.
gcl_length = 16

# Image preprocessing modules
transform = transforms.Compose([
    transforms.Pad(4),
    #transforms.RandomHorizontalFlip(),
    transforms.RandomCrop(32),
    transforms.ToTensor()])

# CIFAR-10 dataset
train_dataset = torchvision.datasets.CIFAR10(root='../../data/',
                                             train=True, 
                                             transform=transforms.ToTensor(),
                                             download=True)

test_dataset = torchvision.datasets.CIFAR10(root='../../data/',
                                            train=False, 
                                            transform=transforms.ToTensor())

# Data loader
train_loader = torch.utils.data.DataLoader(dataset=train_dataset,
                                           batch_size=200, 
                                           shuffle=True)

test_loader = torch.utils.data.DataLoader(dataset=test_dataset,
                                          batch_size=10, 
                                          shuffle=False)



class Hook():
    def __init__(self, module, backward=False):
        if backward==False:
            self.hook = module.register_forward_hook(self.hook_fn)
        else:
            self.hook = module.register_backward_hook(self.hook_fn)
    def hook_fn(self, module, input, output):
        self.input = input
        self.output = output
    def close(self):
        self.hook.remove()

def compute_cams(activations, labels, weights):
    # get number of batches
    cams = []
    gcl_index = []
    batches,_,_,_ = activations.size()
    for b in range(batches):
        cw = weights[labels[b],:] # get class weights for label[b]
        a = activations[b,:]
        # dot product between class weights vector and filter activations
        cam = (cw.view(-1,1,1) * a).sum(0)
        cam = F.relu(cam)
        # normalize
        cam_min, cam_max = cam.min(), cam.max()
        cam = (cam - cam_min).div(cam_max - cam_min)
        cams.append(cam)
        # print('CAM:\n',cam)
        # n,m = cam.size()
        index = cam_to_index(cam.view(-1), gcl_length)
        gcl_index.append(index.item())
        #print("Index: ", index.item())

    return cams, gcl_index


def compute_cam_classes(activations, weights, gcltable):
    # get number of batches
    gcl_index = []
    batches,_,_,_ = activations.size()
    # number of classes in this CNN
    classes, _ = weights.size()
    index_matrix = torch.zeros([batches, classes], dtype=torch.int32)
    probs_matrix = torch.zeros(index_matrix.size(), dtype=torch.float32)
    
    for b in range(batches):
        
        a = activations[b,:]
        # compute cams for each class
        
        for c in range(classes):
            cw = weights[c,:] # get class weights for class c
            # dot product between class weights vector and filter activations
            cam = (cw.view(-1,1,1) * a).sum(0)
            cam = F.relu(cam)
            # normalize
            cam_min, cam_max = cam.min(), cam.max()
            cam = (cam - cam_min).div(cam_max - cam_min)
            index = cam_to_index(cam.view(-1), gcl_length)
            #print("Index: ", index)
            index_matrix[b][c] = index
            # get the likelihood probability
            probs_matrix[b][c] = gcltable[c][index]
    return index_matrix, probs_matrix

# takes a 2D tensor and computes an index for global correlation vector,
# which is a powerset of all positive CAM activations
def cam_to_index(cam, bits):
    # cam_activation_point is the threshhold on cam activations
    a = torch.where(cam > cam_activation_point, 1, 0)
    b = a.view(-1)
    mask = 2 ** torch.arange(bits - 1, -1, -1).to(b.device, b.dtype)
    return torch.sum(mask * b, -1)



if __name__ == '__main__':

    number_classes = 10
    confusion_matrix = torch.zeros(number_classes, number_classes)
    confusion_matrix_fc = torch.zeros(number_classes, number_classes)

    # create the GCL structure, we use ones for Laplace smoothing
    gcl = torch.load('gcml/gcl001NoFlip.pt')

    x,_ = gcl.size()
    for y in range(x):
        gcl[y] = gcl[y] / gcl[y].sum(0)    

    gcl = gcl.to(device)
    model = torch.load('models/res4cifNoFlip.pt')
    model.eval()
    model = model.to(device)
    #print(model)
    hook = Hook(model.layer4[1].bn2)
    z = 0

    total = 0
    correct = 0
    correct_fl = 0
    correct_hybrid = 0

    confusion_matrix = torch.zeros(number_classes, number_classes)
    confusion_matrix_fc = torch.zeros(number_classes, number_classes)

    with torch.no_grad(): 
        for i, (images, labels) in enumerate(test_loader):
            images = images.to(device)
            labels = labels.to(device)
            outputs = model(images)

            # correct on cnn preds
            _, pred = torch.max(outputs.data, 1)
            correct_fl += (pred == labels).sum().item()
            # print("predic: ", pred)
            # print("Labels: ", labels)

            for t, p in zip(pred.view(-1), labels.view(-1)):
                confusion_matrix_fc[t.long(), p.long()] += 1

            #matrix = compute_cams(hook.output, labels, model.fc.weight)
            matrix, probs = compute_cam_classes(hook.output, model.fc.weight, gcl)
            #print("Likelihoods:\n", probs)
            values, predicted = probs.max(1)
            # print("gcml: ", predicted)
            # print(values)
            predicted = predicted.to(device)
            # print(predicted)
            # print(labels)
            # compute accuracy
            total += labels.size(0)
            correct += (predicted == labels).sum().item()

            # confusion matrix calculation
            for t, p in zip(predicted.view(-1), labels.view(-1)):
                confusion_matrix[t.long(), p.long()] += 1

            #break
         # calculate accuracy
        class_names = ['airplane', 'automobile', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck']
        print('G Accuracy: {} %'.format(100 * correct / total))
        print('C Accuracy: {} %'.format(100 * correct_fl / total))
        print("G Matrix: \n", confusion_matrix)
        print("C Matrix: \n", confusion_matrix_fc)
        print("GCML class ACC: ", mx.per_class_accuracy(confusion_matrix))
        print("CNN class ACC: ", mx.per_class_accuracy(confusion_matrix_fc))
        print("GCML F1: ", mx.combinedF1(confusion_matrix))
        print("CNN F1: ", mx.combinedF1(confusion_matrix_fc))

        # display the conf matrix
        cm = confusion_matrix.int()
        u.plot_confusion_matrix(cm, class_names)